package com.activity.a1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class A1Application {

	public static void main(String[] args) {
		SpringApplication.run(A1Application.class, args);
	}


	@GetMapping("/hi")
	public String hi(@RequestParam(value= "name", defaultValue = "user") String name) {
		return String.format("Hi, %s!", name);
	}

	@GetMapping("/goal")
	public String goal(@RequestParam(value= "name", defaultValue = "user") String name, @RequestParam(value= "age", defaultValue = "null") String age) {
		return String.format("Hello %s, Your age is %s!", name, age);
	}


}
